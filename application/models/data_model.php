<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once './application/libraries/my_library/Classes/Message.php';

class Data_model extends CI_Model {

    public function get_messages($start = 0) {
        $messages = $this->db->select()
                ->from('messages')
                ->limit(10, $start)
                ->order_by('date', 'desc')
                ->get()
                ->custom_result_object('Message');
        $this->checkChildren($messages);
        return $messages;
    }

    public function get_comments($id) {
        $comments = $this->db->select()
                ->from('comments')
                ->where('parent_id', $id)
                ->order_by('date')
                ->get()
                ->custom_result_object('Comments');
        $this->checkChildren($comments);
        return $comments;
    }
    public function add ($data, $type){
        $this->db->insert("$type", $data);
    }

    private function checkChildren($messages) {
        foreach ($messages as &$message) {
            $children = $this->db->select()
                    ->from('comments')
                    ->where('parent_id', $message->id)
                    ->get()
                    ->num_rows();
            if ($children > 0) {
                $message->hasChildren = true;
            }
        }
    }

}
