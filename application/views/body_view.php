
<?php foreach ($messages as $message): ?>
    <div class="container message" data-id="<?php echo $message->id; ?>">
        <div class="row">
            <div class="col-md-1">
                <?php if ($message->hasChildren == true) { ?>
                    <button class="btn btn-success opener"><span class="glyphicon glyphicon-arrow-right"><i></i></span></button>
                <?php } ?>
            </div>
            <div class="col-md-11" style="border:black solid 1px">
                <div class="row">
                    <div class="col-md-6">
                        <p>Author: <?php echo $message->author; ?> </p>  
                    </div>
                    <div class="col-md-6">
                        <p>Date: <?php echo $message->date; ?></p>  
                    </div>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p><?php echo $message->message; ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12  text-right">
                        <?php if ($this->session->fb_id != 0) { ?>
                            <button class="btn btn-info reply">reply</button> 
                        <?php } ?>
                    </div>                            
                </div>
                <br>
            </div>
            <br>
        </div>
    </div>
    <br>
<?php endforeach; ?>
<script>
    function constructBlock(a, b) {
        var block = "";
        for (i = 1; i < a.length; i++) {
            block = block + '<div class="container';
            if (b === "message") {
            } else {
                block = block + '-liquid';
            }
            block = block + ' message" data-id="' + a[i].id + '">'
                    + '<div class="row">'
                    + '<div class="col-md-1">';
            if (a[i].hasChildren === true) {
                block = block + '<button class="btn btn-success opener"><span class="glyphicon glyphicon-arrow-right"><i></i></span></button>';
            }
            block = block + '</div>'
                    + '<div class="col-md-11" style="border:black solid 1px">'
                    + '<div class="row">'
                    + '<div class="col-md-6">'
                    + '<p>Author:' + a[i].author + '</p>'
                    + '</div>'
                    + '<div class="col-md-6">'
                    + '<p>Date:' + a[i].date + '</p>'
                    + '</div>'
                    + '<hr>'
                    + '</div>'
                    + '<div class="row">'
                    + '<div class="col-md-12">'
                    + '<p>' + a[i].message + '</p>'
                    + '</div>'
                    + '</div>'
                    + '<div class="row">'
                    + '<div class="col-md-12 text-right">';
            if (a[0] == "true") {
                block = block + '<button class="btn btn-info reply">reply</button>';
            }
            block = block + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '<br>';
        }
        ;
        return block;
    }
    ;</script>

<script>
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(window).scrollTop() >= $(document).height() - $(window).height()) {
                var number = $(".message").last().attr("data-id");
                $.ajax({
                    url: 'http://ltest.ua/index.php/ajax/infscroll',
                    type: 'POST',
                    data: ({
                        id: number
                    }),
                    dataType: 'json',
                    success: function (data) {
                        /*var answer = JSON.parse(data);*/
                        var type = "message";
                        var block = constructBlock(data, type);
                        $("body").append(block);
                    },
                    error: function () {
                        alert("Hughston, we have a data base problem!");
                    }
                });
            }
            ;
        });
    });</script>
<script>

    $(document).on("click", ".opener", function () {
        if ($(this).find("span:first").hasClass("glyphicon-arrow-right")) {
            $(this).find("span:first").removeClass("glyphicon-arrow-right").addClass("glyphicon-arrow-down");
        } else {
            $(this).find("span:first").removeClass("glyphicon-arrow-down").addClass("glyphicon-arrow-right");
        }
        if ($(this).attr("data-loaded")) {
            $(this).parents(".message:first").find(".container-liquid").toggle();
        } else {
            $(this).attr("data-loaded", "true");
            var number = $(this).parents('.message').attr("data-id");
            $.ajax({
                url: 'http://Ltest.ua/index.php/ajax/showChildren',
                type: 'POST',
                data: ({
                    id: number
                }),
                dataType: 'json',
                success: function (data) {
                    var type = "comments";
                    var block = constructBlock(data, type);
                    $('[data-id^="' + number + '"]').find('.col-md-11:first').append(block);
                },
                error: function () {
                    alert("Hughston, we have a data base problem!");
                }
            });
        }

    });</script>
<script>

    $(document).on("click", "#send_message", function () {
        var type = "message";
        var text = $("#message_text").val();
        $.ajax({
            url: 'http://ltest.ua/index.php/ajax/add',
            type: 'POST',
            data: ({
                type: type,
                text: text
            }),
            dataType: 'text',
            success: function (data) {
                if (data.substring(0, 1) === "*") {
                    $("[data-validation-status^='message']").text(data);
                } else {
                    location.reload();
                }
            },
            error: function () {
                alert("Hughston, we have a data base problem!");
            }
        });
    });</script>
<script>

    $(document).on("click", ".reply", function () {

        if ($(this).siblings().length > 0) {
        } else {
            $(this).parent().append('<div id="reply_form" class="row"><br>' + '<div class="col-md-12">' +
                    '<label><b>Input your reply here:</b></label>' +
                    '<br>' +
                    '<textarea id="comments_text" class="form-control" rows="3"></textarea>' + '</div>' +
                    '<div class="row">' +
                    '<div class="col-md-12">' +
                    '<p id="reply" class="server-reply text-danger" ></p>' +
                    '<br>' +
                    '</div>' +
                    '</div>' +
                    '<div class="row">' + '<div class="col-md-12">' +
                    '<button class="btn btn-default" type="button" id="close" >Close</button>' +
                    '<button class="btn btn-success" id="add_reply">Add reply</button>' +
                    '</div>' + '</div>' +
                    '</div>');
        }
    });</script>
<script>

    $(document).on("click", "#close", function () {
        $(this).parents('div#reply_form').remove();
    });</script>
<script>
    $(document).on("click", "#add_reply", function () {
        var type = "comment";
        var text = $("#comments_text").val();
        var parent = $(this).parents("div.message:first").attr("data-id");
        $.ajax({
            url: 'http://ltest.ua/index.php/ajax/add',
            type: 'POST',
            data: ({
                type: type,
                text: text,
                parent: parent
            }),
            dataType: 'text',
            success: function (data) {
                if (data.substring(0, 1) === "*") {
                    $("#reply").text(data);
                } else {
                    location.reload();
                }
            },
            error: function () {
                alert("Hughston, we have a data base problem!");
            }
        });
    });
</script>
</body>
</html>
