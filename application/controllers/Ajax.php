<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

    public function infscroll() {
        $this->load->model('data_model');
        $last = $this->input->post('id');
        if (!isset($last)) {
            show_404();
        }

        $start = $this->db->select()->from('messages')->get()->num_rows() - $last+1;
       if($start>0){
            $dbrequest = $this->data_model->get_messages($start);
        
        if ($this->session->fb_id!=0) {
            array_unshift($dbrequest, "true");
        } else {
            array_unshift($dbrequest, "false");
        }
        $answer = json_encode($dbrequest);
       echo $answer;}
    }

    public function showChildren() {
        $this->load->model('data_model');
        $id = $this->input->post('id');
        if (!isset($id)) {
            show_404();
        }

        $dbrequest = $this->data_model->get_comments($id);
       if ($this->session->fb_id!=0) {
            array_unshift($dbrequest, "true");
        } else {
            array_unshift($dbrequest, "false");
        }
        $answer = json_encode($dbrequest);
        echo $answer;
    }

    public function add() {
        $this->load->model('data_model');
        $text = htmlspecialchars($this->input->post('text'));
        if (!isset($text)) {
            show_404();
        }
        $texttrim = trim($text);
        if (strlen($texttrim) < 10) {
            echo '* Your message is too short. It should contain at least 10 symbols.';
        }
        $type = $this->input->post('type') . 's';
        $data = array(
            
            'author' =>$this->session->name,
            'message' => $texttrim
        );
        if ($type === "comments") {
            $data['parent_id'] = $this->input->post('parent');
        }
        $this->data_model->add($data, $type);
    }

}
