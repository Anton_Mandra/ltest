<?php include './application/config/fb.php'; ?>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function mainpage() {

        if ($this->session->fb_id === null) {
            $this->auth();
        }

        $this->load->model('data_model');
        $this->load->view('head_view');
        
        if($this->session->fb_id != null){
        $this->load->view('messageinput_view');}
        else{
            $this->load->view('auth_view');
        }
        $this->load->view('footer_view');

        $data['messages'] = $this->data_model->get_messages();
        $this->load->view('body_view', $data);
    }

    public function startpage() {


        $this->load->view('start_page_view');
    }

    private function auth() {
        if ($this->input->get('code')) {
            $token_url = 'https://graph.facebook.com/v2.9/oauth/access_token?client_id='
                    . ID . '&redirect_uri='
                    . URL . '&client_secret='
                    . SECRET . '&code='
                    . $this->input->get('code');
            $token = json_decode(file_get_contents($token_url), true);
            $data_url = 'https://graph.facebook.com/v2.9/me?client_id='
                    . ID . '&redirect_uri='
                    . URL . '&client_secret='
                    . SECRET . '&code='
                    . $this->input->get('code')
                    . '&access_token='
                    . $token['access_token'] .
                    '&fields=id,name';

            $session_data = json_decode(file_get_contents($data_url), true);
            $this->session->name = '';
            $this->session->name = $session_data['name'];
            $this->session->fb_id = '';
            $this->session->fb_id = $session_data['id'];
        } else {
            $this->session->name = 'Guest';
        }
    }

}
