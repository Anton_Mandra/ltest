﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.2.78.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 18.02.2018 22:39:05
-- Версия сервера: 5.5.53-log
-- Версия клиента: 4.1
--


-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

-- 
-- Установка базы данных по умолчанию
--
USE LiT;

--
-- Описание для таблицы Comments
--
DROP TABLE IF EXISTS Comments;
CREATE TABLE Comments (
  id INT(11) NOT NULL AUTO_INCREMENT,
  parent_id INT(11) NOT NULL,
  author_id VARCHAR(255) NOT NULL,
  author VARCHAR(255) NOT NULL,
  message VARCHAR(255) NOT NULL,
  date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 204
AVG_ROW_LENGTH = 409
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы Messages
--
DROP TABLE IF EXISTS Messages;
CREATE TABLE Messages (
  id INT(11) NOT NULL AUTO_INCREMENT,
  author_id VARCHAR(255) NOT NULL,
  author VARCHAR(255) NOT NULL,
  message VARCHAR(255) NOT NULL,
  date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 51
AVG_ROW_LENGTH = 327
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- 
-- Вывод данных для таблицы Comments
--
INSERT INTO Comments VALUES
(203, 46, '1', '1', 'dddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', '2018-02-18 21:28:49');

-- 
-- Вывод данных для таблицы Messages
--
INSERT INTO Messages VALUES
(1, '', '', 'ывапыпыпыпыпывывыва', '2018-01-06 21:27:09'),
(2, '', '', 'ывапывапывапвркеоаноаво', '2018-01-07 21:28:24'),
(3, '', '', '1679091c5a880faf6fb5e6087eb1b2dc', '2018-01-08 21:29:12'),
(4, '', '', '8f14e45fceea167a5a36dedd4bea2543', '2018-01-09 21:29:12'),
(5, '', '', '8f14e45fceea167a5a36dedd4bea2543', '2018-01-10 21:29:12'),
(6, '', '', '1679091c5a880faf6fb5e6087eb1b2dc', '2018-01-11 21:29:12'),
(7, '', '', 'c9f0f895fb98ab9159f51fd0297e236d', '2018-01-12 21:29:12'),
(8, '', '', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '2018-01-13 21:29:12'),
(9, '', '', 'cfcd208495d565ef66e7dff9f98764da', '2018-01-14 21:29:12'),
(10, '', '', 'c51ce410c124a10e0db5e4b97fc2af39', '2018-01-15 21:29:12'),
(11, '', '', '9bf31c7ff062936a96d3c8bd1f8f2ff3', '2018-01-16 21:29:12'),
(12, '', '', 'c81e728d9d4c2f636f067f89cc14862c', '2018-01-17 21:29:12'),
(13, '', '', 'cfcd208495d565ef66e7dff9f98764da', '2018-01-18 21:29:12'),
(14, '', '', 'c51ce410c124a10e0db5e4b97fc2af39', '2018-01-19 21:29:12'),
(15, '', '', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '2018-01-20 21:29:12'),
(16, '', '', 'e4da3b7fbbce2345d7772b0674a318d5', '2018-01-21 21:29:12'),
(17, '', '', 'c4ca4238a0b923820dcc509a6f75849b', '2018-01-22 21:29:12'),
(18, '', '', '45c48cce2e2d7fbdea1afc51c7c6ad26', '2018-01-23 21:29:12'),
(19, '', '', 'd3d9446802a44259755d38e6d163e820', '2018-01-24 21:29:12'),
(20, '', '', 'cfcd208495d565ef66e7dff9f98764da', '2018-01-25 21:29:12'),
(21, '', '', 'd3d9446802a44259755d38e6d163e820', '2018-01-26 21:29:12'),
(22, '', '', 'e4da3b7fbbce2345d7772b0674a318d5', '2018-01-27 21:29:12'),
(23, '', '', 'c9f0f895fb98ab9159f51fd0297e236d', '2018-01-28 21:29:12'),
(24, '', '', 'c20ad4d76fe97759aa27a0c99bff6710', '2018-01-29 21:29:12'),
(25, '', '', '8f14e45fceea167a5a36dedd4bea2543', '2018-01-30 21:29:12'),
(26, '', '', 'c20ad4d76fe97759aa27a0c99bff6710', '2018-01-31 21:29:12'),
(27, '', '', 'a87ff679a2f3e71d9181a67b7542122c', '2018-02-01 21:29:12'),
(28, '', '', '8f14e45fceea167a5a36dedd4bea2543', '2018-02-02 21:29:12'),
(29, '', '', 'c51ce410c124a10e0db5e4b97fc2af39', '2018-02-03 21:29:12'),
(30, '', '', '45c48cce2e2d7fbdea1afc51c7c6ad26', '2018-02-04 21:29:12'),
(31, '', '', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '2018-02-05 21:29:12'),
(32, '', '', 'c9f0f895fb98ab9159f51fd0297e236d', '2018-02-06 21:29:12'),
(33, '', '', 'c81e728d9d4c2f636f067f89cc14862c', '2018-02-07 21:29:12'),
(34, '', '', '8f14e45fceea167a5a36dedd4bea2543', '2018-02-08 21:29:12'),
(35, '', '', 'c51ce410c124a10e0db5e4b97fc2af39', '2018-02-09 21:29:12'),
(36, '', '', '9bf31c7ff062936a96d3c8bd1f8f2ff3', '2018-02-10 21:29:12'),
(37, '', '', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '2018-02-11 21:29:12'),
(38, '', '', '1679091c5a880faf6fb5e6087eb1b2dc', '2018-02-12 21:29:12'),
(39, '', '', 'e4da3b7fbbce2345d7772b0674a318d5', '2018-02-13 21:29:12'),
(40, '1', '1', 'sdfgsdfggggg', '2018-02-18 19:57:20'),
(41, '1', '1', 'wwwwwwwwwwwwwwwww', '2018-02-18 19:59:15'),
(42, '1', '1', 'w', '2018-02-18 19:59:21'),
(43, '1', '1', 'g', '2018-02-18 20:00:30'),
(44, '1', '1', 'g', '2018-02-18 20:00:32'),
(45, '1', '1', '', '2018-02-18 21:28:36'),
(46, '1', '1', 'dddddddddddddddddddddddddddddddddddddd', '2018-02-18 21:28:40'),
(47, '', '1985213648398959', 'wwwwwwwwwwwwwwwwwwwww', '2018-02-18 22:32:09'),
(48, '', '1985213648398959', 'wwwwwwwwwwwwwwwwwwwww', '2018-02-18 22:32:09'),
(49, '', 'Anton Mandra', '4444444444444444', '2018-02-18 22:33:53'),
(50, '', 'Anton Mandra', '4444444444444444', '2018-02-18 22:33:54');

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;